<?php

namespace Drupal\group_content_string_id\Extension;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Provides a module uninstall volidator for Group Content with String IDs.
 */
class GroupContentStringIdUninstallValidator implements ModuleUninstallValidatorInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a module uninstall volidator for Group Content with String IDs.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   * @param \Drupal\Core\Database\Connection $connection
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager, Connection $connection, TranslationInterface $stringTranslation) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->connection = $connection;
    $this->setStringTranslation($stringTranslation);
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    $reasons = [];
    if ($module !== 'group_content_string_id') {
      return $reasons;
    }

    $entityTypeId = 'group_content';
    $fieldName = 'entity_id';

    $storage = $this->entityTypeManager->getStorage($entityTypeId);
    assert($storage instanceof SqlEntityStorageInterface);
    $tableMapping = $storage->getTableMapping();

    $storageDefinitions = $this->entityFieldManager->getFieldStorageDefinitions($entityTypeId);
    assert(isset($storageDefinitions[$fieldName]));
    $storageDefinition = $storageDefinitions[$fieldName];
    $columnName = $tableMapping->getFieldColumnName($storageDefinition, $storageDefinition->getMainPropertyName());

    // @todo Use TableMappingInterface::getAllFieldTableNames() once Drupal 9.1
    //   is the minimum supported version.
    foreach ($tableMapping->getTableNames() as $table) {
      if (in_array($fieldName, $tableMapping->getFieldNames($table), TRUE)) {
        $result = $this->connection->select($table, $table)
          ->fields($table, [$columnName])
          ->condition("$table.$columnName", '^[0-9]+$', 'NOT REGEXP')
          ->range(0, 1)
          ->execute()
          ->fetchField();
        if ($result) {
          $reasons[] = $this->t('To uninstall Group Content with String ID, remove all content with string IDs from any groups');
          break;
        }
      }
    }

    return $reasons;
  }

}
