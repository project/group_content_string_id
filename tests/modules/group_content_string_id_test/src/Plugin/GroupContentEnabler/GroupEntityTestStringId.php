<?php

namespace Drupal\group_content_string_id_test\Plugin\GroupContentEnabler;

use Drupal\group\Plugin\GroupContentEnablerBase;

/**
 * Provides a content enabler for test entities with string IDs.
 *
 * @GroupContentEnabler(
 *   id = "group_entity_test_string_id",
 *   label = @Translation("Group test entity with string ID"),
 *   description = @Translation("Adds test entities with string IDs to groups."),
 *   entity_type_id = "entity_test_string_id",
 * )
 */
class GroupEntityTestStringId extends GroupContentEnablerBase {
}
