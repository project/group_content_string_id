<?php

namespace Drupal\Tests\group_content_string_id\Kernel;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorException;
use Drupal\Tests\group\Kernel\GroupKernelTestBase;

/**
 * Tests the installation of the Group Content with String IDs module.
 *
 * @group group_content_string_id
 */
class InstallUninstallTest extends GroupKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'entity_test',
    'group_content_string_id_test',
  ];

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The module installer.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected $moduleInstaller;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->refreshServices();

    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('entity_test_string_id');
    $this->installConfig('group_content_string_id_test');
  }

  /**
   * {@inheritdoc}
   */
  protected function refreshServices() {
    parent::refreshServices();

    $this->database = $this->container->get('database');
    $this->moduleInstaller = $this->container->get('module_installer');
  }

  /**
   * Tests installation and uninstallation without any string ID group content.
   *
   * @throws \Drupal\Core\Extension\ExtensionNameLengthException
   * @throws \Drupal\Core\Extension\MissingDependencyException
   */
  public function testInstallUninstallNoString() {
    // The default group type has both group content with integer IDs (users)
    // and with string IDs (test entities). Create a group for testing. This
    // will automatically add a group content for the membership of the current
    // user.
    $group = $this->createGroup();
    $groupContentTypeId = 'default-group_membership';
    $expected = [
      [
        'gid' => $group->id(),
        'type' => $groupContentTypeId,
        'entity_id' => $this->getCurrentUser()->id(),
      ],
    ];
    $this->assertGroupContentData($expected);

    // Test that the module can be installed.
    $this->moduleInstaller->install(['group_content_string_id']);
    $this->refreshServices();
    $this->assertGroupContentData($expected);

    // Test that integer ID group content can still be added after installation.
    $anotherUser = $this->createUser();
    $group->addMember($anotherUser);
    $expected[] = [
      'gid' => $group->id(),
      'type' => $groupContentTypeId,
      'entity_id' => $anotherUser->id(),
    ];
    $this->assertGroupContentData($expected);

    // Test that the module can be uninstalled.
    $this->moduleInstaller->uninstall(['group_content_string_id']);
    $this->assertGroupContentData($expected);

    // Test that integer ID group content can still be added after
    // uninstallation.
    $thirdUser = $this->createUser();
    $group->addMember($thirdUser);
    $expected[] = [
      'gid' => $group->id(),
      'type' => $groupContentTypeId,
      'entity_id' => $thirdUser->id(),
    ];
    $this->assertGroupContentData($expected);
  }

  /**
   * Tests installation of the module and adding string ID group content.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Extension\ExtensionNameLengthException
   * @throws \Drupal\Core\Extension\MissingDependencyException
   */
  public function testInstallString() {
    // The default group type has both group content with integer IDs (users)
    // and with string IDs (test entities). Create a group for testing with a
    // string ID group content as well as an integer ID group content (the
    // automatic membership of the current user).
    $group = $this->createGroup();
    $testEntity = $this->entityTypeManager->getStorage('entity_test_string_id')->create([
      'id' => $this->randomMachineName(),
    ]);
    $this->assertInstanceOf(ContentEntityInterface::class, $testEntity);
    $testEntity->save();
    $groupContentPluginId = 'group_entity_test_string_id';
    $group->addContent($testEntity, $groupContentPluginId);
    $groupContent = $group->getContentByEntityId($groupContentPluginId, $testEntity->id());
    $this->assertCount(1, $groupContent);
    $groupContent = reset($groupContent);
    $expected = [
      [
        'gid' => $group->id(),
        'type' => 'default-group_membership',
        'entity_id' => $this->getCurrentUser()->id(),
      ],
      [
        'gid' => $group->id(),
        'type' => $groupContent->getContentPlugin()->getContentTypeConfigId(),
        // Without the module, the entity ID is not saved properly.
        'entity_id' => '0',
      ],
    ];
    $this->assertGroupContentData($expected);

    // Remove the group content again and re-add it after installing the module.
    $groupContent->delete();
    array_pop($expected);
    $this->assertGroupContentData($expected);
    $this->moduleInstaller->install(['group_content_string_id']);
    $this->refreshServices();
    $group->addContent($testEntity, $groupContentPluginId);
    $expected[] = [
      'gid' => $group->id(),
      'type' => $groupContent->getContentPlugin()->getContentTypeConfigId(),
      // With the module the entity ID should be saved properly.
      'entity_id' => $testEntity->id(),
    ];
    $this->assertGroupContentData($expected);

    // Make sure that the uninstallation validator prevents uninstallation as
    // long as there is group content with string IDs.
    $this->expectException(ModuleUninstallValidatorException::class);
    $this->expectExceptionMessage('The following reasons prevent the modules from being uninstalled: To uninstall Group Content with String ID, remove all content with string IDs from any groups');
    $this->moduleInstaller->uninstall(['group_content_string_id']);
  }

  /**
   * Asserts that the group content database in the database is as expected.
   *
   * @param array $expected
   *   The expected group content data as an array of arrays where each inner
   *   array contains the following keys:
   *   gid: The group ID.
   *   type: The group content type ID.
   *   entity_id: The entity ID.
   */
  protected function assertGroupContentData(array $expected) {
    $result = $this->database->select('group_content_field_data', 'gcfd')
      ->fields('gcfd', ['gid', 'type', 'entity_id'])
      ->orderBy('gid')
      ->orderBy('type')
      ->orderBy('entity_id')
      ->execute()
      ->fetchAll();
    $actual = [];
    foreach ($result as $row) {
      $actual[] = (array) $row;
    }
    $this->assertSame($expected, $actual);
  }

}
